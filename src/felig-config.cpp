/*******************************************************************/
/*                                                                 */
/* C++ source code for felig-config tool                             */
/*                                                                 */
/* Author: Ablet, CERN , email: yiming.abulaiti@cern.ch            */
/*                                                                 */
/** Jan 2019. Get some code from flx-config tool  ******************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <iostream>
#include <iomanip>
#include "felixtag.h"
#include "DFDebug/DFDebug.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include <getopt.h>

using namespace std;

#define APPLICATION_NAME    "felig-config"

//usage
static void print_help(){

    printf ("-d, %-10s %s\n", "N","Use device number N. [Default: 0]");
    printf ("-v, %-10s %s\n", "--verbose","Print additional debug information.");
    printf ("-h, %-10s %s\n", "--help","Print help and exit.");
    printf ("-V, %-10s %s\n", " ","Print version and exit.");
    printf("--chunk_length\n \t%s\n \t%s\n \t%s\n",
           "Chunk Length in hex [bytes].",
           "Must pad with 4 '0's",
           "Default: --chunk_length=0x3C"
           );
#if REGMAP_VERSION < 0x500
    printf("--endian_mode\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "Elink endian mode. One bit per egroup.",
           "0x00 =  little-endian",
           "0x1F =  big-endian",
           "default: 0x00"
           );
    printf("--data_format\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "Elink data format. One bit per egroup.",
           "0x1F =  8b10b mode",
           "0x00 =  direct mode",
           "default: 0x1F"
           );
    printf("--elink_input_width\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "Elink input data width. One bit per egroup.",
           "0x00 =  8-bit (direct mode)",
           "0x1F =  10-bit (8b10b mode)",
           "default: 0x1F"
           );
    printf("--elink_output_width\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "Elink output data width. One bit per egroup.",
           "For Each E-Group",
           "0x0 = all e-links set to 2 bit",
           "0x1 = all e-links set to 4-bit",
           "0x2 = all e-links set to 8-bit",
           "0x3 = all e-links set to 16-bit",
           "default: 0x250"
           );
    printf("--elink_enable\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "For Each E-Group",
           "0x00 = all e-links disabled",
           "0xFF = all e-links enabled, for 2-bit width",
           "0x55 = all e-links enabled, for 4-bit width",
           "0x11 = all e-links enabled, for 8-bit width",
           "0x01 = all e-links enabled, for 16-bit width",
           "default: 0x115555FF00"
           );
#else
    printf("--endian_mode\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "Elink endian mode. One bit per egroup.",
           "0x00 =  little-endian",
           "0x7F =  big-endian",
           "default: 0x00"
           );
    printf("--data_format\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "Elink data format. Two bits per egroup.",
           "0x3FFF =  AURORA (lpGBT only)",
           "0x1555 =  8b10b mode",
           "0x0000 =  direct mode",
           "default: 0x1555"
           );
    printf("--elink_input_width\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "Elink input data width. One bit per egroup.",
           "0x00 =  8-bit (direct mode)",
           "0x7F =  10-bit (8b10b mode)",
           "default: 0x7F"
           );
    printf("--elink_output_width\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "Elink output data width. Three bits per egroup.",
           "For Each E-Group",
           "0x0 = e-link set to 2 bit (GBT only)",
           "0x1 = e-link set to 4-bit (GBT only)",
           "0x2 = e-link set to 8-bit",
           "0x3 = e-link set to 16-bit (lpGBT only)",
	   "0x4 = e-link set to 32-bit (lpGBT only)",
           "default GBT: 0x002240   (8b 4b 4b 2b 2b)",
           "default lpGBT: 0x1246D2 (32b 32b 32b 16b 16b 8b 8b)"
           );
    printf("--elink_enable\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "For Each E-Group",
           "GBT:",
           "  0x00 = all e-links disabled",
           "  0xFF = all e-links enabled, for 2-bit width",
           "  0x55 = all e-links enabled, for 4-bit width",
           "  0x11 = all e-links enabled, for 8-bit width",
           "  0x01 = all e-links enabled, for 16-bit width",
           "  default: 0x115555FFFF (8b 4b 4b 2b 2b)",
	   "lpGBT (only 28 bits used):",
           "  0x0 = all e-links disabled",
           "  0xF = all e-links enabled, for 8-bit width",
           "  0x5 = all e-links enabled, for 16-bit width",
           "  0x1 = all e-links enabled, for 32-bit width",
           "  default: 0x00011155FF (32b 32b 32b 16b 16b 8b 8b)"
           );
#endif // REGMAP_VERSION 
    printf("--b_channel_select\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "TTC Config. Select b-channel data bit from GBT word.",
           "0x21 = Elink0 - 2-bit TTC",
           "0x23 = Elink0 - 4-bit TTC",
           "0x27 = Elink0 - 8-bit TTC",
           "0xFF = unused");
    exit(0);
}

//Globals
FlxCard flxCard;

//Debug
bool debug=false;

//Total number of channels
int channels=24;

// Chunk Length in hex [bytes].
// Must pad with 4 '0's
//chunk_length="0x20"
const char *chunk_length="0x3C";

// Elink endian mode
// One bit per egroup
// 0x00 =  little-endian
// 0x1F =  big-endian RM4
// 0x7F =  big-endian RM5
const char *endian_mode="0x00";

// Elink data format
#if REGMAP_VERSION < 0x500
// One bit per egroup
// 0x1F =  8b10b mode
// 0x00 =  direct mode
const char *data_format="0x1F";
#else
// Two bit per egroup
// 0x3FFF =  AURORA (lpGBT only) 
// 0x1555 =  8b10b mode
// 0x0000 =  direct mode
const char *data_format="0x1555";
#endif // REGMAP_VERSION 

// Elink input data width
//One bit per egroup
// 0x00 =  8-bit (direct mode)
#if REGMAP_VERSION < 0x500
// 0x1F =  10-bit (8b10b mode)
const char *elink_input_width="0x1F";
#else
// 0x7F =  10-bit (8b10b mode)
const char *elink_input_width="0x7F";
#endif // REGMAP_VERSION 

//// Elink output data width
#if REGMAP_VERSION < 0x500
// Two bits per egroup
// For Each E-Group
// 0x0 = all e-links set to 2 bit
// 0x1 = all e-links set to 4 bit
// 0x2 = all e-links set to 8 bit
// 0x3 = all e-links set to 16 bit
//default GBT: 0x250   8b 4b 4b 2b 2b
const char *elink_output_width="0x250";
#else
// Three bits per egroup
// For Each E-Group
// 0x0 = e-link set to 2 bit (GBT only)
// 0x1 = e-link set to 4 bit (GBT only)
// 0x2 = e-link set to 8 bit 
// 0x3 = e-link set to 16 bit (lpGBT only)
// 0x4 = e-link set to 32 bit (lpGBT only)
//default GBT: 0x002240   8b 4b 4b 2b 2b
//default lpGBT: 0x1246D2 32b 32b 32b 16b 16b 8b 8b
//const char *elink_output_width="0x002240";
const char *elink_output_width="0x1246D2";
#endif // REGMAP_VERSION 

//// Elink Enable
// Following example values assume all elinks are enabled
// within an egroup for a given elink width.
//
//For Each E-Group
#if REGMAP_VERSION < 0x500
// 0x00 = all e-links disabled
// 0xFF = all e-links enabled, for 2-bit width
// 0x55 = all e-links enabled, for 4-bit width
// 0x11 = all e-links enabled, for 8-bit width
// 0x01 = all e-links enabled, for 16-bit width
// default: 0x115555FFFF (8b 4b 4b 2b 2b)
const char *elink_enable="0x115555FFFF";
#else
//GBT:
// 0x00 = all e-links disabled
// 0xFF = all e-links enabled, for 2-bit width
// 0x55 = all e-links enabled, for 4-bit width
// 0x11 = all e-links enabled, for 8-bit width,
// 0x01 = all e-links enabled, for 16-bit width
// default: 0x115555FFFF (8b 4b 4b 2b 2b)
//lpGBT (only 28 bits used):
// 0x0 = all e-links disabled
// 0xF = all e-links enabled, for 8-bit width
// 0x5 = all e-links enabled, for 16-bit width
// 0x1 = all e-links enabled, for 32-bit width
// default: 0x00011155FF (32b 32b 32b 16b 16b 8b 8b)
//const char *elink_enable="0x115555FFFF";
const char *elink_enable="0x00011155FF";
#endif // REGMAP_VERSION 

// As configured:
/* E-Group 1 = All e-links disabled
# E-Group 2 = all (2-bit) e-links enabled
# E-Group 3 = all (4-bit) e-links enabled
# E-Group 4 = all (4-bit) e-links enabled
# E-Group 5 = all (8-bit) e-links enabled

### TTC Config
# Select b-channel data bit from GBT word.
#
# examples:
# "0x01" = Elink0 - 2-bit TTC
# "0x03" = Elink0 - 4-bit TTC
# "0x07" = Elink0 - 8-bit TTC
# "0xFF" = unused
#b_channel_select="0x01"
 */
const char *b_channel_select="0x21";


//Setting global controls
void set_global_control(const char *key_char, const char *value_c){
    u_long converted_value =0;
    sscanf(value_c, "%lx", &converted_value);

    if(debug){
        cout<<key_char<<" = "<<std::hex<<converted_value<<"(base 16) | "<<std::dec<<converted_value<<" (base 10)"<<endl;
    }
    flxCard.cfg_set_option(key_char, converted_value);

}

void set_global_control_all(){

    if(debug){
        cout<<"Setting global controls..."<<endl;
    }

    //set local trigger period. [25ns / LSB]
    set_global_control("FELIG_GLOBAL_CONTROL_FAKE_L1A_RATE", "0x00000000" );
    set_global_control("FELIG_GLOBAL_CONTROL_PICXO_OFFSET_PPM", "0x00000" );
    set_global_control("FELIG_GLOBAL_CONTROL_TRACK_DATA", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_TRACK_DATA", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_RXUSERRDY", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_TXUSERRDY", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_AUTO_RESET", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_PICXO_RESET", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_GTTX_RESET", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_CPLL_RESET", "1" );
    set_global_control("FELIG_GLOBAL_CONTROL_X3_X4_OUTPUT_SELECT", "0x00" );

    //Release Global Resets
    set_global_control("FELIG_GLOBAL_CONTROL_PICXO_RESET", "0" );
    set_global_control("FELIG_GLOBAL_CONTROL_GTTX_RESET", "0" );
    set_global_control("FELIG_GLOBAL_CONTROL_CPLL_RESET", "0" );

    //Toggle Lane Resets
    set_global_control("FELIG_RESET_LANE", "0xFFFFFF" );
    set_global_control("FELIG_RESET_LANE", "0x000000" );

    //Toggle framegen resets.  (shouldn't be required.)
    set_global_control("FELIG_RESET_FRAMEGEN", "0xFFFFFF" );
    set_global_control("FELIG_RESET_FRAMEGEN", "0x000000" );

    //Toggle Loopback FIFO Resets
    set_global_control("FELIG_RESET_LB_FIFO", "0xFFFF" );
    set_global_control("FELIG_RESET_LB_FIFO", "0x0000" );

    //Toggle GBT RX Resets
    //Now performed in flx-init
    //set_global_control("GBT_GTRX_RESET", "0xFFFFFFFFFF" );
    //set_global_control("GBT_GTRX_RESET", "0x0000000000" );
}

//Get this from flx-confit.cpp
void set_option_ch(const char *key_char, const char *c_value, u_short ch = 0){

    char key[255];
    snprintf(key, 255, key_char, ch);

    //string ll;
    u_long value = strtoll(c_value, NULL, 0);

    if(debug){
        cout<<key<<" = "<<std::hex<<value<<"(base 16) | "<<std::dec<<value<<"(base 10)"<<endl;
    }

    try{
        flxCard.cfg_set_option(key, value);
        //cout<<key<<" = "<<value<<endl;
    }
    catch (FlxException &excep){
        cout << "ERROR. Exception thrown: " << excep.what() << endl;
        exit(-1);
    }

    //delete[] key;
}

//appy setup for 24 ch
void set_option_all (){
    for (u_short i=0; i<channels; i++) {
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_USERDATA","0x0000", i);
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_CHUNK_LENGTH",chunk_length, i);
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_RESET","0x00", i);
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_SW_BUSY","0x00", i);
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_DATA_FORMAT", data_format, i);
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_PATTERN_SEL","0x00", i);
        set_option_ch("FELIG_ELINK_CONFIG_%02u_ENDIAN_MOD", endian_mode, i);
        set_option_ch("FELIG_ELINK_CONFIG_%02u_INPUT_WIDTH", elink_input_width, i);
        set_option_ch("FELIG_ELINK_CONFIG_%02u_OUTPUT_WIDTH", elink_output_width, i);
        set_option_ch("FELIG_ELINK_ENABLE_%02u", elink_enable, i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_LB_FIFO_DELAY", "0x02", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_ELINK_SYNC", "0", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_PICXO_OFFEST_EN", "0", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_PI_HOLD", "0", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_GBT_LB_ENABLE", "0", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_GBH_LB_ENABLE", "0", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_FG_SOURCE", "0", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_B_CH_BIT_SEL", b_channel_select, i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_A_CH_BIT_SEL", "0x7F", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_L1A_SOURCE", "1", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_GBT_EMU_SOURCE", "0", i);


    }

}

//Toggle Lane Resets
void resets(){
    set_option_ch("FELIG_RESET_LANE", "0xFFFFFF");
    set_option_ch("FELIG_RESET_LANE", "0x000000");

    //Toggle framegen resets.  (shouldn't be required.)
    set_option_ch("FELIG_RESET_FRAMEGEN", "0xFFFFFF");
    set_option_ch("FELIG_RESET_FRAMEGEN", "0x000000");

    //Toggle Loopback FIFO Resets
    set_option_ch("FELIG_RESET_LB_FIFO", "0xFFFF");
    set_option_ch("FELIG_RESET_LB_FIFO", "0x0000");
}

void elink_sync(){
    for (int i=0; i<channels; i++) {
        set_option_ch("FELIG_LANE_CONFIG_%02u_ELINK_SYNC", "0x1", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_ELINK_SYNC", "0x0", i);
    }
}

//main function
int main(int argc /*Number of arguments*/, char **argv/*Pointer array to arguments*/){

    //parse options
    int opt=0;
    int device_number = 0;

    //loop over argv[] and read option strings
    while ( 1 ){
        int option_index = 0;
        static struct option long_options[] = {
            {"chunk_length",     required_argument, 0,  'c' },
            {"endian_mode",     required_argument, 0,  'm' },
            {"data_format",     required_argument, 0,  'f' },
            {"elink_input_width",     required_argument, 0,  'i' },
            {"elink_output_width",     required_argument, 0,  'o' },
            {"elink_enable",     required_argument, 0,  'e' },
            {"b_channel_select",     required_argument, 0,  'b' },
            {0,         0,                 0,  0 }
        };

        opt = getopt_long(argc, argv, "hd:Vv", long_options, &option_index);
        if(opt==-1) break;

        switch (opt){
            case 'h':
                print_help();
                break;

            case 'd':
                device_number = atoi(optarg);
                break;

            case 'V':
                break;

            case 'v':
                debug=true;
                break;
            case 'c':
                chunk_length = optarg;
                break;
            case 'm':
                endian_mode = optarg;
                break;
            case 'f':
                data_format = optarg;
                break;
            case 'i':
                elink_input_width = optarg;
                break;
            case 'o':
                elink_output_width = optarg;
                break;
            case 'e':
                elink_enable = optarg;
                break;
            case 'b':
                b_channel_select = optarg;
                break;
            default:{
                    //should print something.
                    print_help();
                }
                break;
        }//end of switch

    }//end of while loop


    try
    {
        flxCard.card_open(device_number, LOCK_ALL);
    }
    catch(FlxException &ex)
    {
        std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
        exit(-1);
    }


    //FELIG global configuration
    set_global_control_all();

    //FELIG lane configuration
    set_option_all();

    // Toggle Lane Resets
    resets();

    //FELIG LANE CONFIG -- ELINK_SYNC
    elink_sync();

    try
    {
        flxCard.card_close();
    }
    catch(FlxException &ex)
    {
        std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
        exit(-1);
    }

}// end of main()
