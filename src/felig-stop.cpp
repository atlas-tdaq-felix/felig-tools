/*******************************************************************/
/*                                                                 */
/* C++ source code for felig-stop tool                             */
/*                                                                 */
/* Author: Ablet, CERN , email: yiming.abulaiti@cern.ch            */
/*                                                                 */
/** Jan 2019. Get some code from flx-config tool  ******************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <iomanip>
#include "felixtag.h"
#include "DFDebug/DFDebug.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"

using namespace std;

#define APPLICATION_NAME    "felig-stop"

//usage
static void print_help(){

    printf ("-d, %-10s %s\n", "N","Use device number N. [Default: 0]");
    printf ("-v, %-10s %s\n", "--verbose","Print additional debug information.");
    printf ("-h, %-10s %s\n", "--help","Print help and exit.");
    printf ("-V, %-10s %s\n", " ","Print version and exit.");
    printf("-c DESC, %-10s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n",
           " --channels=DESC",
           "The command applies to the specified channels.",
           "Channels are counted from 1. DESC can be:",
           "- A channel. Example: 5.",
           "- A range. Example: 1-4.",
           "- A list of channels. Example: 1,4,6.",
           "- A combination of the above.",
           "Example: 1-3,5,9.",
           "- The word 'all'. This applies the command to all available channels.",
           "Default: all.");
}

//Globals
FlxCard flxCard;

//Debug
bool debug=false;

//Total number of channels
const int channels=24;



//Get this from flx-confit.cpp
void set_option_ch(const char *key_char, const char *c_value, u_short ch = 0){

    char key[255];
    snprintf(key, 255, key_char, ch);

    //string ll;
    u_long value = strtoll(c_value, NULL, 0);

    if(debug){
        cout<<key<<" = "<<std::hex<<value<<"(base 16) | "<<std::dec<<value<<"(base 10)"<<endl;
    }

    try{
        flxCard.cfg_set_option(key, value);
        //cout<<key<<" = "<<value<<endl;
    }
    catch (FlxException &excep){
        cout << "ERROR. Exception thrown: " << excep.what() << endl;
        exit(-1);
    }

    //delete[] key;
}

//delimiter can be spacified with WordDelimitedBy<";">
template<char delimiter>
class WordDelimitedBy : public std::string
{};

//Overwrite >> with template
template<char delimiter>
//Initilize delimiter with WordDelimitedBy<delimiter>
std::istream& operator>>(std::istream& is, WordDelimitedBy<delimiter>& output)
{
    std::getline(is, output, delimiter);
    return is;
}


//main function
int main(int argc /*Number of arguments*/, char **argv/*Pointer array to arguments*/){

    //parse options
    int opt=0;
    int device_number = 0;

    //channel list
    string ch_opts = "all";

    //loop over argv[] and read option strings
    while (1){
        int option_index = 0;
        static struct option long_options[] = {
            {"channels",     required_argument, 0,  'c' },
            {0,         0,                 0,  0 }
        };

        opt = getopt_long(argc, argv, "hd:Vvc:", long_options, &option_index);
        if(opt==-1) break;

        switch (opt){
            case 'h':
                print_help();
                break;

            case 'd':
                device_number = atoi(optarg);
                break;

            case 'V':
                break;

            case 'v':
                debug=true;
                break;

            case 'c':
                ch_opts = optarg;
                break;

            default:
                //should print something.
                //print_help();
                break;
        }//end of switch

    }//end of while loop


    try
    {
        flxCard.card_open(device_number, LOCK_ALL);
    }
    catch(FlxException &ex)
    {
        std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
        exit(-1);
    }


    size_t pos = ch_opts.find_first_of("all", 0);
    vector<u_short> ch_vector;
    if(pos != std::string::npos){
        //all channels
        for (u_short i=0; i<channels; i++) {
            ch_vector.push_back(i);
        }
    } else {
        // not all channels.
        std::istringstream iss(ch_opts);
        char read_c;
        while(iss>>read_c){ //check if there is any unexpected character.
            if (!isdigit(read_c) && (read_c != '-') && (read_c !=',')){
                cerr<<"Wrong input format of channel list!"<<endl;
                exit(-1);

            }

        }
        iss = std::istringstream(ch_opts);
        //Initilize vector from string stream. >> operator is overwriten.
        std::vector<std::string> results((std::istream_iterator<WordDelimitedBy<','>>(iss)),
                                         std::istream_iterator<WordDelimitedBy<','>>());
        //loop over results, stor selected channels.
        for (auto &i : results){
            if ( !isdigit(i[0]) ){//check against "-8"
                cerr<<"Can not recognize channel  "<<i<<"!"<<endl;
                exit(-1);
            }
            char* pp;
            u_short iCh = strtol(i.c_str(), &pp, 10); //convert str to int long using 10 base
            if (iCh>23 ){ //Max channels are 24.
                cerr<<"Range of channels are [0-23]!  "<<iCh<<" is given."<<endl;
                exit(-1);
            }
            if (*pp){ //range of channels
                if ( !isdigit(i[i.size()-1]) ){ //last character should be number, "8-".
                    cerr<<"Can not recognize channel  "<<i<<"!"<<endl;
                    exit(-1);
                }
                u_short r_end = atoi(pp+1);
                if (r_end>23 ){// check the upper boundary is in [0-23].
                    cerr<<"Range of channels are [0-23]!  "<<i<<" is given."<<endl;
                    exit(-1);
                }
                for(u_short i = iCh; i< r_end+1; i++){
                    ch_vector.push_back(i);
                }
            }
            else{// a single channel? save it to the list.
                ch_vector.push_back(iCh);
            }

        }//end for loop

    }//end of channel list parsing.

    if(debug){
        cout<<"Channels: "<<endl;
        for(auto &iv :  ch_vector){
            cout<<iv<<" ";
        }
        cout<<endl;
    }

    //confit all channels
    for (unsigned int i=0; i < ch_vector.size(); i++) {
        set_option_ch("FELIG_LANE_CONFIG_%02u_A_CH_BIT_SEL", "0x7F", ch_vector.at(i) );
        //set_option_ch("FELIG_LANE_CONFIG_%02u_GBT_EMU_SOURCE", gbt_emu_source, ch_vector.at(i) );

    }

    //confit all channels
    for (unsigned int i=0; i<ch_vector.size(); i++) {
        set_option_ch("FELIG_LANE_CONFIG_%02u_L1A_SOURCE", "1", ch_vector.at(i) );
    }

    try
    {
        flxCard.card_close();
    }
    catch(FlxException &ex)
    {
        std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
        exit(-1);
    }

}// end of main()
