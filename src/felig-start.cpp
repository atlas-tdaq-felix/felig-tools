/*******************************************************************/
/*                                                                 */
/* C++ source code for felig-start tool                             */
/*                                                                 */
/* Author: Ablet, CERN , email: yiming.abulaiti@cern.ch            */
/*                                                                 */
/** Jan 2019. Get some code from flx-config tool  ******************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <iomanip>
#include "felixtag.h"
#include "DFDebug/DFDebug.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"

using namespace std;

#define APPLICATION_NAME    "felig-start"

//usage
static void print_help(){

    printf ("-d, %-10s %s\n", "N","Use device number N. [Default: 0]");
    printf ("-v, %-10s %s\n", "--verbose","Print additional debug information.");
    printf ("-h, %-10s %s\n", "--help","Print help and exit.");
    printf ("-V, %-10s %s\n", " ","Print version and exit.");
    printf("-c DESC, %-10s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n",
           " --channels=DESC",
           "The command applies to the specified channels.",
           "Channels are counted from 1. DESC can be:",
           "- A channel. Example: 5.",
           "- A range. Example: 1-4.",
           "- A list of channels. Example: 1,4,6.",
           "- A combination of the above.",
           "Example: 1-3,5,9.",
           "- The word 'all'. This applies the command to all available channels.",
           "Default: all.");
    printf("--local-trigger\n \t%s\n \t%s\n \t%s\n",
           "Use a FELIG-generated trigger based on a local",
           "counter. If this option is not given, FELIG",
           "expects that a trigger signal is sent by FELIX.");
    printf("--ttc-a-channel=X \n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "Select the TTC A-channel data bit from the GBT word. X can be:",
           "0x20, for E-Link 0, 2-bit TTC",
           "0x22, for E-Link 0, 4-bit TTC",
           "0x26, for E-Link 0, 8-bit TTC",
           "0xFF, for unset",
           "Default: 0x20");
    printf("--emu=X \n \t%s\n \t%s\n \t%s\n \t%s\n",
           "Select the source of emulator data. Possible values are:",
           "- sm, for a state-machine based emulator",
           "- ram, for a RAM-based emulator",
           "Default: sm");
    printf("--rate=X[kHz/MHz]\n \t%s\n \t%s\n \t%s\n \t%s\n",
           "Sets the local trigger rate to X. X is given",
           "in Hz. The [kHz] or [MHz] can be used for",
           "kHz or MHz, respectively.",
           "Default: 100[kHz]");

    printf("--Random_chunk_length\n \t%s\n",
           "Use random chunk length.");
    printf("--MSB\n \t%s\n \t%s\n",
           "Switch to MSB",
           "Default is LSB.");
}

//Globals
FlxCard flxCard;

//Debug
bool debug=false;

//Total number of channels
const int channels=24;



//Get this from flx-config.cpp
void set_option_ch(const char *key_char, const char *c_value, u_short ch = 0){

    char key[255];
    snprintf(key, 255, key_char, ch);

    //string ll;
    u_long value = strtoll(c_value, NULL, 0);

    if(debug){
        cout<<key<<" = "<<std::hex<<value<<"(base 16) | "<<std::dec<<value<<"(base 10)"<<endl;
    }

    try{
        flxCard.cfg_set_option(key, value);
        //cout<<key<<" = "<<value<<endl;
    }
    catch (FlxException &excep){
        cout << "ERROR. Exception thrown: " << excep.what() << endl;
        exit(-1);
    }

    //delete[] key;
}

//delimiter can be spacified with WordDelimitedBy<";">
template<char delimiter>
class WordDelimitedBy : public std::string
{};

//Overwrite >> with template
template<char delimiter>
//Initilize delimiter with WordDelimitedBy<delimiter>
std::istream& operator>>(std::istream& is, WordDelimitedBy<delimiter>& output)
{
    std::getline(is, output, delimiter);
    return is;
}

//convert rate to period.
//0x190=400=40Mhz/100kHZ
u_long ratetoperiod(string test_str){

    char* pos;
    u_long rate = strtoll(test_str.c_str(), &pos,0);
    u_long unit = 1;
    if(*pos){
        auto begin = test_str.find_first_of('[');
        auto end = test_str.find_first_of(']',begin+1);
        string unit_str = test_str.substr(begin+1,end-begin-1);

        if(unit_str == "Hz"){
            unit = 1;
        }else if (unit_str == "kHz"){
            unit = 1000;
        }else if (unit_str == "MHz"){
            unit = 1000000;
        }else {
            cerr<<"Cannot recognize unit "<<unit<<"!"<<endl;
            exit(-1);
        }
    }
    rate = rate * unit;
    u_long period = 40000000/rate;
    return period;
}

//main function
int main(int argc /*Number of arguments*/, char **argv/*Pointer array to arguments*/){

    //parse options
    int opt=0;
    int device_number = 0;

    // L1A ID Source / Trigger Mode
    //#
    //# 0 = local trigger with local counter
    //# 1 = from FELIX
    const char* l1a_source="1";

    // TTC Config
    //# Select a-channel data bit from GBT word.
    //#
    //# examples:
    //# "0x20" = Elink0 - 2-bit TTC
    //# "0x22" = Elink0 - 4-bit TTC
    //# "0x26" = Elink0 - 8-bit TTC
    //# "0xFF" = unused
    const char* a_channel_select="0x20";

    // set local trigger period. [25ns / LSB]
    //l1a_rate=0x00010000
    const char* l1a_rate="0x0000190";

    // Emulation Data Source
    //#
    //# 0 = state-machine emulator
    //# 1 = ram-based emulator.
    const char* gbt_emu_source="0";

    //Randome chuck length.
    //0 = constant length
    //1 = random length
    const char* Random_chunk_length="0";

    //channel list
    string ch_opts = "all";

    //MSB option
    //0 --> LSB
    //1 --> MSB
    const char*  MSB = "0";

    //loop over argv[] and read option strings
    while (1){
        int option_index = 0;
        static struct option long_options[] = {
            {"channels",     required_argument, 0,  'c' },
            {"local-trigger",     no_argument, 0,  'l' },
            {"Random_chunk_length",     no_argument, 0,  'R' },
            {"rate",     required_argument, 0,  'r' },
            {"emu",     required_argument, 0,  'e' },
            {"ttc-a-channel",     required_argument, 0,  'a' },
            {"MSB",     no_argument, 0,  'M' },
            {0,         0,                 0,  0 }
        };

        opt = getopt_long(argc, argv, "hd:Vvc:lr:e:a:", long_options, &option_index);
        if(opt==-1) break;

        switch (opt){
            case 'h':
                print_help();
                break;

            case 'd':
                device_number = atoi(optarg);
                break;

            case 'V':
                break;

            case 'v':
                debug=true;
                break;

            case 'c':
                ch_opts = optarg;
                break;

            case 'l':
                l1a_source = "0";
                break;
            case 'R':
                Random_chunk_length = "1";
                break;
            case 'M':
                MSB = "1";
                break;

            case 'r':{
                string tmp_rate =std::to_string(ratetoperiod(optarg));
                l1a_rate = (char* )tmp_rate.data();
            }
                break;

            case 'e':
                {
                    string optarg_str = optarg;
                    if (optarg_str == "sm"){
                        gbt_emu_source = "0";
                    }else if (optarg_str == "ram"){
                        gbt_emu_source = "1";
                    }else {
                        cerr<<"Unknown emulator "<<optarg_str<<". Choose sm or ram!"<<endl;
                        exit(-1);
                    }

                }
                break;

            case 'a':
		a_channel_select = optarg;
                //{
                    //string ttc_channle = optarg;
                    //if(ttc_channle == "2"){
                    //    a_channel_select = "0x20";
                    //}else if (ttc_channle == "4"){
                    //    a_channel_select = "0x03";
                    //}else if (ttc_channle == "8"){
                    //    a_channel_select = "0x07";
                    //}else if (ttc_channle == "none"){
                    //    a_channel_select = "0xFF";
                    //}else {
                    //    cerr<<"Unknown ttc A-channel channle"<<ttc_channle<<endl;
                    //    exit(-1);
                    //}

                //}
                break;
            default:
                //should print something.
                //print_help();
                break;
        }//end of switch

    }//end of while loop


    try
    {
        flxCard.card_open(device_number, LOCK_ALL);
    }
    catch(FlxException &ex)
    {
        std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
        exit(-1);
    }


    size_t pos = ch_opts.find_first_of("all", 0);
    vector<u_short> ch_vector;
    if(pos != std::string::npos){
        //all channels
        for (u_short i=0; i<channels; i++) {
            ch_vector.push_back(i);
        }
    } else {
        // not all channels.
        std::istringstream iss(ch_opts);
        char read_c;
        while(iss>>read_c){ //check if there is any unexpected character.
            if (!isdigit(read_c) && (read_c != '-') && (read_c !=',')){
                cerr<<"Wrong input format of channel list!"<<endl;
                exit(-1);

            }

        }
        iss = std::istringstream(ch_opts);
        //Initilize vector from string stream. >> operator is overwriten.
        std::vector<std::string> results((std::istream_iterator<WordDelimitedBy<','>>(iss)),
                                         std::istream_iterator<WordDelimitedBy<','>>());
        //loop over results, stor selected channels.
        for (auto &i : results){
            if ( !isdigit(i[0]) ){//check against "-8"
                cerr<<"Can not recognize channel  "<<i<<"!"<<endl;
                exit(-1);
            }
            char* pp;
            u_short iCh = strtol(i.c_str(), &pp, 10); //convert str to int long using 10 base
            if (iCh>23 ){ //Max channels are 24.
                cerr<<"Range of channels are [0-23]!  "<<iCh<<" is given."<<endl;
                exit(-1);
            }
            if (*pp){ //range of channels
                if ( !isdigit(i[i.size()-1]) ){ //last character should be number, "8-".
                    cerr<<"Can not recognize channel  "<<i<<"!"<<endl;
                    exit(-1);
                }
                u_short r_end = atoi(pp+1);
                if (r_end>23 ){// check the upper boundary is in [0-23].
                    cerr<<"Range of channels are [0-23]!  "<<i<<" is given."<<endl;
                    exit(-1);
                }
                for(u_short i = iCh; i< r_end+1; i++){
                    ch_vector.push_back(i);
                }
            }
            else{// a single channel? save it to the list.
                ch_vector.push_back(iCh);
            }

        }//end for loop

    }//end of channel list parsing.

    if(debug){
        cout<<"Channels: "<<endl;
        for(auto &iv :  ch_vector){
            cout<<iv<<" ";
        }
        cout<<endl;
    }

    // set local trigger period. [25ns / LSB]
    set_option_ch("FELIG_GLOBAL_CONTROL_FAKE_L1A_RATE", l1a_rate);

    set_option_ch("FMEMU_RANDOM_CONTROL_SELECT_RANDOM", Random_chunk_length, 0 );

#if REGMAP_VERSION < 0x500
    set_option_ch("CR_REVERSE_10B_FROMHOST", MSB, 0 );
#else
    set_option_ch("ENCODING_REVERSE_10B", MSB, 0 );
#endif // REGMAP_VERSION

    //config all channels
    for (unsigned int i=0; i < ch_vector.size(); i++) {
        set_option_ch("FELIG_LANE_CONFIG_%02u_A_CH_BIT_SEL", a_channel_select, ch_vector.at(i) );
        set_option_ch("FELIG_LANE_CONFIG_%02u_GBT_EMU_SOURCE", gbt_emu_source, ch_vector.at(i) );

    }

    //config all channels
    for (unsigned int i=0; i<ch_vector.size(); i++) {
        set_option_ch("FELIG_LANE_CONFIG_%02u_L1A_SOURCE", l1a_source, ch_vector.at(i) );
    }

    try
    {
        flxCard.card_close();
    }
    catch(FlxException &ex)
    {
        std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
        exit(-1);
    }

}// end of main()
