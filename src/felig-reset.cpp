/*******************************************************************/
/*                                                                 */
/* C++ source code for felig-config tool                             */
/*                                                                 */
/* Author: Ablet, CERN , email: yiming.abulaiti@cern.ch            */
/*                                                                 */
/** Jan 2019. Get some code from flx-config tool  ******************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <iostream>
#include <iomanip>
#include "felixtag.h"
#include "DFDebug/DFDebug.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"
#include <getopt.h>

using namespace std;

#define APPLICATION_NAME    "felig-reset"

//usage
static void print_help(){

    printf ("-d, %-10s %s\n", "N","Use device number N. [Default: 0]");
    printf ("-v, %-10s %s\n", "--verbose","Print additional debug information.");
    printf ("-h, %-10s %s\n", "--help","Print help and exit.");
    //printf ("-V, %-10s %s\n", " ","Print version and exit.");
    printf("--reset\n \t%s\n \t%s\n \t%s\n",
           "Reset registers",
           "options: all, data_gen_config, l1id, elink_sync",
           "Default: all. reset all three registers."
           );

    exit(0);
}

//Globals
FlxCard flxCard;

//Debug
bool debug=false;

//Total number of channels
int channels=24;


//Get this from flx-confit.cpp
void set_option_ch(const char *key_char, const char *c_value, u_short ch = 0){

    char key[255];
    snprintf(key, 255, key_char, ch);

    //string ll;
    u_long value = strtoll(c_value, NULL, 0);

    if(debug){
        cout<<key<<" = "<<std::hex<<value<<"(base 16) | "<<std::dec<<value<<"(base 10)"<<endl;
    }

    try{
        flxCard.cfg_set_option(key, value);
        //cout<<key<<" = "<<value<<endl;
    }
    catch (FlxException &excep){
        cout << "ERROR. Exception thrown: " << excep.what() << endl;
        exit(-1);
    }

    //delete[] key;
}

//appy setup for 24 ch
void reset_data_gen_config (){
    for (u_short i=0; i<channels; i++) {
#if REGMAP_VERSION < 0x500
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_RESET","0x1F", i);
#else
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_RESET","0x7F", i);
#endif // REGMAP_VERSION
        set_option_ch("FELIG_DATA_GEN_CONFIG_%02u_RESET","0x00", i);
    }
}

//Toggle Lane Resets
void reset_l1id(){
    set_option_ch("FELIG_L1ID_RESET", "0");
}

void reset_elink_sync(){
    for (int i=0; i<channels; i++) {
        set_option_ch("FELIG_LANE_CONFIG_%02u_ELINK_SYNC", "0x1", i);
        set_option_ch("FELIG_LANE_CONFIG_%02u_ELINK_SYNC", "0x0", i);
    }
}

//main function
int main(int argc /*Number of arguments*/, char **argv/*Pointer array to arguments*/){

    //parse options
    int opt=0;
    int device_number = 0;
    string reset_opt = "all";
    //loop over argv[] and read option strings
    while ( 1 ){
        int option_index = 0;
        static struct option long_options[] = {
            {"reset",     required_argument, 0,  'R' },
            {0,         0,                 0,  0 }
        };

        opt = getopt_long(argc, argv, "hd:v", long_options, &option_index);
        if(opt==-1) break;

        switch (opt){
            case 'h':
                print_help();
                break;

            case 'd':
                device_number = atoi(optarg);
                break;
            case 'v':
                debug=true;
                break;
            case 'R':
                reset_opt = optarg;
                break;
            default:{
                    //should print something.
                    print_help();
                }
                break;
        }//end of switch

    }//end of while loop


    try
    {
        flxCard.card_open(device_number, LOCK_ALL);
    }
    catch(FlxException &ex)
    {
        std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
        exit(-1);
    }

    if (debug){
      cout<<"reset option is: "<< reset_opt<<endl;
    }
    if ((reset_opt == "all") || ( reset_opt == "data_gen_config")){
      //FELIG lane configuration
      cout<<"data gen config reset"<<endl;
      reset_data_gen_config();
    }

    if((reset_opt == "all") || ( reset_opt == "l1id")){
      cout<<"reset L1ID counter"<<endl;
      reset_l1id();
    }


    if((reset_opt == "all") || ( reset_opt == "elink_sync")){
      //FELIG LANE CONFIG -- ELINK_SYNC
      cout<<"FELIG LANE CONFIG XX  ELINK_SYNC reset"<<endl;
      reset_elink_sync();
    }

    if( (reset_opt != "all") && ( reset_opt != "elink_sync") && ( reset_opt != "l1id") && ( reset_opt != "data_gen_config") ){
      cerr<<"Unknown reset options "<< reset_opt<< ". Check possible options with -h."<<endl;
      exit(-1);
    }

    try
    {
        flxCard.card_close();
    }
    catch(FlxException &ex)
    {
        std::cout << "ERROR. Exception thrown: " << ex.what() << std:: endl;
        exit(-1);
    }

}// end of main()
