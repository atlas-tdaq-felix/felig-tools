/* C++ source code for felig-status tool                             */
/*                                                                 */
/* Author: Ablet, CERN , email: yiming.abulaiti@cern.ch            */
/*                                                                 */
/** Jan 2019. Get some code from flx-config tool  ******************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <iomanip>
#include "felixtag.h"
#include "DFDebug/DFDebug.h"
#include "flxcard/FlxCard.h"
#include "flxcard/FlxException.h"

using namespace std;

#define APPLICATION_NAME    "felig-status"

//usage
static void print_help(){

    printf ("-d, %-10s %s\n", "N","Use device number N. [Default: 0]");
    printf ("-v, %-10s %s\n", "--verbose","Print additional debug information.");
    printf ("-h, %-10s %s\n", "--help","Print help and exit.");
    printf ("-V, %-10s %s\n", " ","Print version and exit.");

}

//Globals
FlxCard flxCard;
u_long baraddr2;
//Debug
bool debug=false;


/*******************************/
//this is from flx-info.cpp
static void display_card_id(void)
/*******************************/
{
    u_long card_id = 0, reg_map_version = 0, major, minor;
    int card_control = 0;

    printf("Card type        : ");
    card_id = flxCard.cfg_get_option(BF_CARD_TYPE);
    if(card_id == 712)
    {
        printf("FLX-712\n");
        card_control = 1;
    }
    if(card_id == 711)
    {
        printf("FLX-711\n");
        card_control = 1;
    }
    if(card_id == 710)
    {
        printf("FLX-710\n");
        card_control = 1;
    }
    if(card_id == 709)
    {
        printf("FLX-709\n");
        card_control = 1;
    }
    if(card_control == 0)
        printf("UNKNOWN\n");

    // Register map version stored as hex: 0x0300 -> 3.0
    reg_map_version = flxCard.cfg_get_option(BF_REG_MAP_VERSION);
    major = (reg_map_version & 0xFF00) >> 8;
    minor = (reg_map_version & 0x00FF) >> 0;
    printf("Reg Map version  : %lx.%lx\n", major, minor);
}


/********************************/
//this is from flx-info.cpp
static void display_channels(void)
/********************************/
{
    u_long channels = 0;

    channels = flxCard.cfg_get_option(BF_NUM_OF_CHANNELS);
    printf("Number of channels    : %lu\n", channels);
}

/**************************************/
//this is from flx-info.cpp
static void display_channels_alignment()
/**************************************/
{
    u_long cont, channels = 0, result = 0, mask = 1, number_channels = 0;

    channels = flxCard.cfg_get_reg(REG_GBT_ALIGNMENT_DONE);
    number_channels = flxCard.cfg_get_option(BF_NUM_OF_CHANNELS);
    u_long card_type = flxCard.cfg_get_reg(REG_CARD_TYPE);

    if(debug)
    {
        printf("Number of channels: %lu\n", number_channels);
        printf("Content of REG_GBT_ALIGNMENT_DONE: 0x%lx\n", channels);
    }

    printf("\nGBT CHANNEL ALIGNMENT STATUS");
    if( card_type == 712 || card_type == 711 )
        printf( " (entire FLX-712/711):" );

    int group = number_channels;
    if( card_type == 712 || card_type == 711 )
        number_channels *= 2;
    // Split list of channels into groups per card (for 712/711)
    for( u_long i=0; i<(number_channels+group-1)/group; ++i )
    {
        u_long min = i * group;
        u_long max = min + group;
        if( max > number_channels ) max = number_channels;

        printf("\nChannel |");
        for (cont = min; cont < max; cont++)
            printf(" %2lu  ", cont);
        printf("\n        ");
        for (cont = min; cont < max; cont++)
            printf("-----");

        printf("\nAligned |");
        for(cont = min; cont < max; cont++)
        {
            mask = (1ul << cont);

            result = channels & mask;
            if(result >= 1)
                printf(" YES ");
            else
                printf(" NO  ");
        }
        printf( "\n" );
    }
    printf("\n");
}

/****************************/
//this is from flx-config.cpp
static void print_header(void)
/****************************/
{
    printf("Offset  RW Bits                                         Name               Value  Description\n");
    printf("================================================================================================================\n");
}


/**********************************************************************************************************************************/
//this is from flx-config.cpp
static void print_register(u_long address, u_int flags, u_int lo, u_int hi, const char *name, u_long value, const char *description)
/**********************************************************************************************************************************/
{
    const int NAME_SPAN   = 42;
    const int DESC_INDENT = 7+11+NAME_SPAN+2+2+16+2;
    if(flags & REGMAP_REG_READ)
    {
        cout << setfill('0');
        cout << hex  << "0x" << setw(4) << address << dec;

        cout << " [";

        if (hi > 64)
        {
            cout << "TRIGGER ";
            lo = hi;
        }
        else
        {
            if (flags & REGMAP_REG_READ)
                cout << "R";
            else
                cout << " ";

            if (flags & REGMAP_REG_WRITE)
                cout << "W ";
            else
                cout << "  ";

            if (hi == lo)
                cout << "   " << setw(2) << lo;
            else
                cout << setw(2) << hi << ":" << setw(2) << lo;
        }

        cout << "] ";
        cout << setfill(' ') << setw(NAME_SPAN) << name;
        // Take into account that some names exceed NAME_SPAN in size
        int nospaces = 0;
        if (strlen(name) > NAME_SPAN )
            nospaces = strlen(name)-NAME_SPAN;

        u_int nblanks;
        if (hi > 64)
            nblanks = 15;
        else if (hi == lo)
            nblanks = 15;
        else
            nblanks = 16 - ((hi-lo+1+3) / 4);
        if (nblanks-nospaces >= 0)
            nblanks -= nospaces;

        cout << setfill(' ') << setw(nblanks) << "";
        cout << "  0x" << setfill('0') << setw((hi-lo+1+3)/4) << hex << value << dec;
        cout << "  ";

        int desc_len = strlen( description );
        const char *ch = description;
        int cnt = 0;
        cout << setfill(' ');
        for( int i=0; i<desc_len; ++i, ++ch ) {
            if( i == desc_len-1 && *ch == '\n' ) // Skip a final newline
                break;
            cout << *ch;
            ++cnt;
            // In case of newline or long line continue on the next line, with indentation
            if( (*ch == '\n') || (cnt > 60 && *ch == ' ') ) {
                if( *ch == ' ' ) cout << endl; // Start next line only after a space
                cout << setw(DESC_INDENT) << " ";
                cnt = 0; // Reset line width counter
            }
        }
        cout << endl;
    }
    else
    {

        cout << hex  << "0x" << address << dec;
        cout << " [";

        if (flags & REGMAP_REG_READ)
            cout << "R";
        else
            cout << " ";

        if (flags & REGMAP_REG_WRITE)
            cout << "W ";
        else
            cout << "  ";

        cout << setfill('0') << setw(2) << hi << ":" << setfill('0') << setw(2) << lo;
        cout << "] ";
        cout << setfill(' ') << setw(NAME_SPAN) << name;
        cout << "                      " << description;
        cout << endl;
    }
}





u_long get_frequency_RX(u_short ch = 0){
    //string reg_str=string("FELIG_MON_FREQ_%02u_RX");
    char key_RX[255];
    snprintf(key_RX, 255, "FELIG_MON_FREQ_%02u_RX", ch);

    u_long value;
    value = flxCard.cfg_get_option(key_RX);
    if(debug){
        printf("%s=0x%lx\n", key_RX, value);
    }
    return value;
}

u_long get_frequency_TX(u_short ch = 0){
    //string reg_str=string("FELIG_MON_FREQ_%02u_TX");
    char key_TX[255];
    snprintf(key_TX, 255, "FELIG_MON_FREQ_%02u_TX", ch);

    u_long value;
    value = flxCard.cfg_get_option(key_TX);
    if(debug){
        printf("%s=0x%lx\n", key_TX, value);
    }

    return value;
}

void show_frequencies(){
    ios state(nullptr);
    state.copyfmt(cout); // save current formatting
    for (u_short ch =0; ch<24; ch++){
        cout<<"LANE "<<setw(2)<<setfill('0')<<ch<<" RX : "<<get_frequency_RX(ch)<<" Hz"<<endl;
        cout<<"LANE "<<setw(2)<<setfill('0')<<ch<<" TX : "<<get_frequency_TX(ch)<<" Hz"<<endl;
        cout<<"-------------------------------------------"<<endl;
    }
    cout.copyfmt(state); // restore previous formatting
}

//main function
int main(int argc /*Number of arguments*/, char **argv/*Pointer array to arguments*/){

    //parse options
    int opt=0;
    int device_number = 0;
    u_long card_model = 0;

    //loop over argv[] and read option strings
    while ( ( opt = getopt(argc, argv, "hd:Vv") ) !=-1 ){

        switch (opt){
            case 'h':
                print_help();
                break;

            case 'd':
                device_number = atoi(optarg);
                break;

            case 'V':
                break;

            case 'v':
                debug=true;
                break;

            default:
                //should print something.
                //print_help();
                break;
        }//end of switch

    }//end of while loop

    try
    {
        flxCard.card_open(device_number, 0);
        baraddr2 = flxCard.openBackDoor(2);
    }
    catch(FlxException &ex)
    {
        std::cout << "ERROR. Exception thrown: " << ex.what() << std::endl;
        exit(-1);
    }

    card_model = flxCard.card_model();
    if(card_model != 712 && card_model != 711 && card_model != 710 && card_model != 709)
    {
        fprintf(stderr, APPLICATION_NAME " error: Card model not recognized\n");
        exit(-1);
    }

    display_card_id();
    display_channels();
    display_channels_alignment();

    show_frequencies();


    try
    {
        flxCard.card_close();
    }
    catch(FlxException &ex)
    {
        std::cout << "ERROR. Exception thrown: " << ex.what() << std::endl;
        exit(-1);
    }


}
