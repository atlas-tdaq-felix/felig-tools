# felig-tools

Jira ticket:
https://its.cern.ch/jira/browse/FLX-802

# install  

Checkout from this repository. Put under software/ directory.  

run cmake_config x86***   

cd x86**/ and build with make.  

 

I have been using this for FELIG <--> test.  

example commands:  
  
./felig-config -d 0  
./felig-config -d 1  
./felig-start --emu=sm --local-trigger -d 0  
./felig-start --emu=sm --local-trigger -d 1  
  
./felig-tools/felig-stop -d 0  
./felig-tools/felig-stop -d 1  

./felig-reset -d N --reset all
  --reset
	options: all, data_gen_config, l1id, elink_sync
	default is "all"

